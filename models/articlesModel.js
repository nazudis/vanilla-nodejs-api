let articles = require('../data/articles.json')
const shortID = require('shortid')
const {
  writeDataToFile
} = require('../utils')

function findAll() {
  return new Promise((resolve, reject) => {
    resolve(articles)
  })
}

function findById(id) {
  return new Promise((resolve, reject) => {
    const article = articles.find((artc) => artc.id == id)
    resolve(article)
  })
}

function create(artc) {
  return new Promise((resolve, reject) => {
    const newArticle = {
      id: shortID.generate(),
      ...artc
    }
    articles.push(newArticle)

    writeDataToFile('./data/articles.json', articles)
    resolve(newArticle)
  })
}

function update(id, artc) {
  return new Promise((resolve, reject) => {

    const index = articles.findIndex((art) => art.id == id)
    articles[index] = {
      id,
      ...artc
    }

    writeDataToFile('./data/articles.json', articles)
    resolve(articles[index])
  })
}

function remove(id) {
  return new Promise((resolve, reject) => {
    articles = articles.filter((artc) => artc.id != id)
    writeDataToFile('./data/articles.json', articles)
    resolve()
  })
}

module.exports = {
  findAll,
  findById,
  create,
  update,
  remove
}