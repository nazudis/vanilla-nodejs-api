const Articles = require('../models/articlesModel')
const {
  getPostData
} = require('../utils')

// @desc    Gets All Article List
// @route   GET /api/articles
async function getArticles(req, res) {
  try {
    const articles = await Articles.findAll()

    res.writeHead(200, {
      'Content-Type': 'application/json'
    })
    res.end(JSON.stringify(articles))
  } catch (error) {
    console.log(error)
  }
}

// @desc    Gets Single Article
// @route   GET /api/articles/:id
async function getArticleById(req, res, id) {
  try {
    const article = await Articles.findById(id)

    if (!article) {
      res.writeHead(404, {
        'Content-Type': 'application/json'
      })
      res.end(JSON.stringify({
        message: 'Article not found'
      }))
    } else {
      res.writeHead(200, {
        'Content-Type': 'application/json'
      })
      res.end(JSON.stringify(article))
    }

  } catch (error) {
    console.log(error)
  }
}

// @desc    Create Article
// @route   POST /api/articles
async function createArticle(req, res) {
  try {
    let body = await getPostData(req)

    const {
      author,
      date_created,
      content,
      images
    } = JSON.parse(body)

    const article = {
      author,
      date_created,
      content,
      images
    }

    const newArticle = await Articles.create(article)

    res.writeHead(201, {
      'Content-Type': 'application/json'
    })
    return res.end(JSON.stringify(newArticle))
  } catch (error) {
    console.log(error)
  }
}

// @desc    Update a Article
// @route   PUT /api/articles/:id
async function updateArticle(req, res, id) {
  try {
    const article = await Articles.findById(id)

    if (!article) {
      res.writeHead(404, {
        'Content-Type': 'application/json'
      })
      res.end(JSON.stringify({
        message: 'Article Not Found'
      }))
    } else {
      const body = await getPostData(req)
      console.log(body)

      const {
        author,
        date_created,
        content,
        images
      } = JSON.parse(body)

      const articleData = {
        author: author || article.author,
        date_created: date_created || article.date_created,
        content: content || article.content,
        images: images || article.images
      }

      const updArticle = await Articles.update(id, articleData)

      res.writeHead(200, {
        'Content-Type': 'application/json'
      })
      return res.end(JSON.stringify(updArticle))
    }

  } catch (error) {
    console.log(error)
  }
}

// @desc    Delete Article
// @route   DELETE /api/articles/:id
async function deleteArticle(req, res, id) {

  try {
    const article = await Articles.findById(id)

    if (!article) {
      res.writeHead(404, { 'Content-Type': 'application/json' })
      res.end(JSON.stringify({ message: 'Article not found' }))

    } else {
      await Articles.remove(id)
      res.writeHead(200, { 'Content-Type': 'application/json' })
      res.end(JSON.stringify({ message: 'success to delete article' }))
    }

  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  getArticles,
  getArticleById,
  createArticle,
  updateArticle,
  deleteArticle
}