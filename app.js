const http = require('http')
const PORT = process.env.PORT || 5000

const {
  getArticles,
  getArticleById,
  createArticle,
  updateArticle,
  deleteArticle
} = require('./controllers/articlesController')

const server = http.createServer((req, res) => {

  if (req.url === '/api/articles' && req.method === 'GET') {
    getArticles(req, res)

  } else if (req.url.match(/\/api\/articles\//) && req.method === 'GET') {
    const id = req.url.split('/')[3]
    getArticleById(req, res, id)

  } else if (req.url === '/api/articles' && req.method === 'POST') {
    createArticle(req, res)

  } else if (req.url.match(/\/api\/articles\//) && req.method === 'PUT') {
    const id = req.url.split('/')[3]
    updateArticle(req, res, id)
  } else if (req.url.match(/\/api\/articles\//) && req.method === 'DELETE') {
    const id = req.url.split('/')[3]
    deleteArticle(req, res, id)
  } else {

    res.writeHead(404, {
      'Content-Type': 'application/json'
    })
    res.end(JSON.stringify({
      message: 'Route not found'
    }))

  }
})

server.listen(PORT, () => console.log(`Server running on port ${PORT}`))